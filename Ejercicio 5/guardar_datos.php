<?php
if (isset($_POST['titulo']) && isset($_POST['director']) && isset($_POST['genero']))
{
    $titulo = $_POST['titulo'];
    $director = $_POST['director'];
    $genero = $_POST['genero'];

    if (!is_file("datos.txt"))
        touch("datos.txt");

    if (is_writable("datos.txt"))
    {
        $archivo = fopen("datos.txt", "at");

        fwrite($archivo, "Título: $titulo" . PHP_EOL);
        fwrite($archivo, "Director: $director" . PHP_EOL);
        fwrite($archivo, "Género: $genero" . PHP_EOL);

        fclose($archivo);

        echo "Datos guardados";
    }
    else
        echo "Imposible guardar los datos";
}
else
{
    echo 'Introduzca primero los datos<br>';
    
    echo '<a href="pelicula.html">Volver</a>';
}
?>

