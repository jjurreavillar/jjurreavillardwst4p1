<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>EJERCICIO 3: LIBRO DE VISITAS</title>
        <style>
            p:first-word {
                font-weight: 600;
            }
        </style>
    </head>
    <body>
        <h1>Libro de visitas</h1>
        
        <form action="Comentarios.php" id="formulario" method="post">
            <label>Tu comentario:</label><br>
            <textarea form="formulario" name="comentario" cols="80" rows="8"></textarea><br>
            <label>Tu nombre:</label><br>
            <input type="text" name="nombre"><br>
            <label>Tu e-mail:</label><br>
            <input type="text" name="email"><br>
            <input type="submit" value="publicar">
        </form>
        
        <h2>Mostrar todos los comentarios</h2>
        
<?php
if (is_file("comentarios.txt"))
{
    $lineas = file("comentarios.txt");

    for($i = 0; $i < count($lineas); $i++)
    {
        echo '<label>'.trim($lineas[$i]).'</label> ('.trim($lineas[++$i]).') escrito el '.trim($lineas[++$i]).':<br>';
        echo trim($lineas[++$i]) . '<br><br>';
    }
}
?>
    </body>
</html>

