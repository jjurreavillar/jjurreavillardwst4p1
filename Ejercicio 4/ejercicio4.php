<?php
if (isset($_POST['nombre']))
    $nombre = $_POST['nombre'];
if (isset($_POST['telefono']))
    $telefono = $_POST['telefono'];
if (isset($_POST['matriculado']))
    $matriculado = $_POST['matriculado'];
if (isset($_POST['enseñanza']))
    $enseñanza = $_POST['enseñanza'];
if (isset($_POST['mostrar']))
    $mostrar = $_POST['mostrar'];

if ($mostrar === "Por Pantalla")
{
    echo "<p> El alumno $nombre, con tel&eacute;fono $telefono, ";
    
    if ($matriculado != "on")
        echo "no ";

    echo "est&aacute; matriculado en ";

    if ($enseñanza == "Ciclo medio" || $enseñanza == "Ciclo superior")
        echo "un ";

    echo "$enseñanza</p>";
}
else
{
    $archivo = fopen("datos.txt", "at");

    fwrite($archivo, $nombre . PHP_EOL);
    fwrite($archivo, $telefono . PHP_EOL);
    fwrite($archivo, $matriculado . PHP_EOL);
    fwrite($archivo, $enseñanza . PHP_EOL);

    fclose($archivo);

    echo '<a href="datos.php">mostrar archivo</a>';
}
?>

