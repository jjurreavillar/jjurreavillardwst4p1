<?php
define("DIR_SUBIDAS", "subidas");

if (isset($_FILES['archivo']['tmp_name']) && is_uploaded_file($_FILES['archivo']['tmp_name']) && $_FILES['archivo']['size'] > 0 && $_FILES['archivo']['size'] <= 200000 && getimagesize($_FILES['archivo']['tmp_name']))
{
    $archivoTemporal = $_FILES['archivo']['tmp_name'];
    $archivo = DIR_SUBIDAS . "/" . time() . "_" . $_FILES['archivo']['name'];

    if (!is_dir(DIR_SUBIDAS) && !file_exists(DIR_SUBIDAS))
        mkdir(DIR_SUBIDAS);

    move_uploaded_file($archivoTemporal, $archivo);
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>EJERCICIO 2: ÁLBUM DE FOTOS</title>
        <style>
            br {
                clear: left;
            }
            img {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
            img[name=thumbnail] {
                border-style: solid;
                display: inline-block;
                margin: 5px;
                margin-right: 0;
                width: 150px;
            }
            div {
                text-align: center;
            }
        </style>
    </head>
    <body onload="AjustarImagenes();">
<?php
$sinSubidas = true;
if (is_dir("subidas"))
{
    $directorio = opendir(DIR_SUBIDAS);

    while ($entrada = readdir($directorio))
        if (is_file(DIR_SUBIDAS . "/" . $entrada))
        {
            if ($sinSubidas)
                echo '<img src="' . DIR_SUBIDAS . '/' . $entrada . '" id="imagen"/><br><div>';
            
            $sinSubidas = false;

            echo '<img src="' . DIR_SUBIDAS . '/' . $entrada . '" name="thumbnail" onclick="VerImagen(event);"/>';
        }

    if (!$sinSubidas)
        echo "</div>";
}
if ($sinSubidas)
    echo "<label>No se han encontrado subidas disponibles.</label>";
?>
        <br>
        <br>
        <a href="album.php">Volver al formulario</a>
        <script>
            function AjustarImagenes()
            {
                var imagen = document.getElementById("imagen");

                if (imagen.height > 650)
                    imagen.height = 650;

                var thumbs = document.getElementsByName("thumbnail");
                
                for(var i = 0; i < thumbs.length; i++)
                {
                }
            }
            function VerImagen(event)
            {
                event = event || window.event;
                var thumb = event.target || event.srcElement;
                
                var imagen = document.getElementById("imagen");
                imagen.src = thumb.src;
                
                AjustarImagenes();
            }
        </script>
    </body>
</html>

